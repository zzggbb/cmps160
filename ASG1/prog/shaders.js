// Basic Vertex Shader that receives position and size for each vertex (point).
var ASSIGN1_VSHADER =
  `attribute vec4 a_Position;
   void main(){
     gl_Position = a_Position;
   }`;

// Basic Fragment Shader that receives a single one color (point).
var ASSIGN1_FSHADER =
  `precision mediump float;
   uniform vec4 u_FragColor;
   void main(){
     gl_FragColor = u_FragColor;
   }`;
