var active_mode = 'circles'
var mouse_is_down = false

function initEventHandlers(gl, canvas, shapes, colors) {
  var clear_button = document.getElementById('clear')
  clear_button.onclick = function(){
    clearCanvas(gl, shapes, colors)
  }

  document.getElementById('squares').onclick = function(){
    active_mode = 'squares'
  }

  document.getElementById('triangles').onclick = function(){
    active_mode = 'triangles'
  }

  document.getElementById('circles').onclick = function(){
    active_mode = 'circles'
  }

  canvas.onmousedown = function(ev) {
    mouse_is_down = true
    click(ev, gl, canvas, shapes, colors)
  }

  canvas.onmousemove = function(ev) {
    if (mouse_is_down)
      click(ev, gl, canvas, shapes, colors)
  }

  canvas.onmouseup = function() {
    mouse_is_down = false
  }
}

function getVertices(x, y) {
  var corners = getNCorners()
  var size = getShapeSize()
  var vertices = [x, y]

  for (var corner = 0; corner < corners; corner++) {
    var angle = 2 * Math.PI * corner / corners
    vertices.push(x + Math.cos(angle) * size)
    vertices.push(y + Math.sin(angle) * size)
  }
  vertices.push(vertices[2])
  vertices.push(vertices[3])
  return vertices
}

function getNCorners() {
  switch (active_mode) {
    case 'squares': return 4
    case 'triangles': return 3
    case 'circles':
      return 1 * document.getElementById('segments_input').value
  }
}

function getShapeSize(){
  return 1 * document.getElementById('shape_size').value
}

/* Function called upon mouse click or mouse drag. Computes position of cursor,
 * pushes cursor position as GLSL coordinates */
function click(ev, gl, canvas, shapes, colors) {
  var x = ev.clientX
  var y = ev.clientY
  var rect = ev.target.getBoundingClientRect()

  var x = ((x - rect.x) - (rect.width / 2)) / (rect.width/2)
  var y = ((rect.height/2) - (y - rect.y)) / (rect.height/2)

  console.log('click event: ' + 'x=' + x + ',y=' + y)

  colors.push([
    document.getElementById('red_color').value,
    document.getElementById('green_color').value,
    document.getElementById('blue_color').value,
    1.0
  ])

  shapes.push(getVertices(x, y))

  render(gl, shapes, colors)
}

function render(gl, shapes, colors) {
  drawBackground(gl)

  if (shapes.length != colors.length) {
    console.log('number of point != number of colors')
    return
  }

  // for each shape
  for (i = 0; i < shapes.length; i++) {
    // send uniform variable 'u_FragColor'
    sendUniformVec4ToGLSL(gl, colors[i], 'u_FragColor')

    var vertices = shapes[i]
    initVertexBuffers(gl, vertices)
    gl.drawArrays(gl.TRIANGLE_FAN, 0, vertices.length / 2)
  }
}

function clearCanvas(gl, shapes, colors) {
  shapes.length = 0
  colors.length = 0
  drawBackground(gl)
}

function drawBackground(gl) {
  gl.clearColor(0.0, 0.0, 0.0, 1.0)
  gl.clear(gl.COLOR_BUFFER_BIT)
}
