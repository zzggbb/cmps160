/* Sends vector to the specified uniform variable within GLSL shaders.
 * @param {Object} gl GL context object
 * @param {Array} val Array (vector) being passed to uniform variable
 * @param {String} uniformName The name of the uniform variable */
function sendUniformVec4ToGLSL(gl, val, uniformName) {
  var loc = gl.getUniformLocation(gl.program, uniformName)
  gl.uniform4f(loc, val[0], val[1], val[2], val[3])
}

/* Sends a vector to the specified attribute within GLSL shaders.
 * @param {Object} gl GL context object
 * @param {Array} val Array (vector) being passed to attribute
 * @param {String} attributeName The name of the attribute*/
function sendAttributeVec2ToGLSL(gl, val, attributeName) {
  var loc = gl.getAttribLocation(gl.program, attributeName)
  gl.vertexAttrib3f(loc, val[0], val[1], 0.0)
}
