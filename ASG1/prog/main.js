function main() {
  var canvas = document.getElementById('webgl')
  var gl = getWebGLContext(canvas)

  colors = []
  shapes = []

  initShaders(gl, ASSIGN1_VSHADER, ASSIGN1_FSHADER)
  clearCanvas(gl, shapes, colors)
  initEventHandlers(gl, canvas, shapes, colors)
}

function initVertexBuffers(gl, vertices){
  var vertices = new Float32Array(vertices)
  var vertexBuffer = gl.createBuffer()
  if (!vertexBuffer) {
    console.log('failed to create vertex buffer')
    return
  }

  gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer)
  gl.bufferData(gl.ARRAY_BUFFER, vertices, gl.STATIC_DRAW)
  var a_Position = gl.getAttribLocation(gl.program, 'a_Position')
  if (a_Position < 0) {
    console.log('failed to get storage location of a_Position')
    return
  }

  gl.vertexAttribPointer(a_Position, 2, gl.FLOAT, false, 0, 0)
  gl.enableVertexAttribArray(a_Position)
}
