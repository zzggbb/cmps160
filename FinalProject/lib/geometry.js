class Geometry {
  constructor(vertices, colors, x, y) {
    this.vertices = vertices || []
    this.colors = colors || []
    this.origin = new Float32Array([x, y, 0, 0])
    this.modelMatrix = new Matrix4()

    this.mode = gl.TRIANGLES
  }
  getModelMatrix() {
    var m = new Matrix4()
    m.setIdentity()
    return m.elements
  }
  update(){
    return
  }
  render() {
    var program = createShaderProgram(gl, VSHADER, FSHADER)

    this.update()

    var [vertices, indices] = interleaveVertexData(this.vertices)
    if (this.indices) indices = this.indices
    indices = new Uint16Array(indices)

    if (this.colors.length == 0) {
      for (var i=0; i<vertices.length/3; i++) {
        for (var j=0; j<3; j++) {
          this.colors.push(Math.random())
        }
        this.colors.push(1.0)
      }
    }

    var vertices_colors = mergeVertsColors(vertices, this.colors)
    gl.bufferData(gl.ARRAY_BUFFER, vertices_colors, gl.STATIC_DRAW)

    var a_Position = gl.getAttribLocation(program, 'a_Position')
    var a_Color = gl.getAttribLocation(program, 'a_Color')
    var FSIZE = vertices_colors.BYTES_PER_ELEMENT
    gl.vertexAttribPointer(a_Position, 3, gl.FLOAT, false, 7*FSIZE, 0)
    gl.vertexAttribPointer(a_Color, 4, gl.FLOAT, false, 7*FSIZE, 3*FSIZE)
    gl.enableVertexAttribArray(a_Position)
    gl.enableVertexAttribArray(a_Color)

    gl.uniform4fv(gl.getUniformLocation(program, 'u_Origin'), this.origin)

    var matrix = new Float32Array(this.getModelMatrix())
    gl.uniformMatrix4fv(gl.getUniformLocation(program, 'u_ModelMatrix'), false, matrix)

    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indices, gl.STATIC_DRAW)
    gl.drawElements(this.mode, indices.length, gl.UNSIGNED_SHORT, 0)
  }
}
