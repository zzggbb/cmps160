function linscale(x, x1, x2, y1, y2) {
  return (y2-y1)/(x2-x1) * (x - x1) + y1;
}

function bound(v, min, max) {
  if (v > max) v = max
  if (v < min) v = min
  return v
}
function mergeVertsColors(v, c) {
  var vc = []
  for (var i=0; i<v.length/3; i++) {
    for (var j=0; j<3; j++) vc.push(v[i*3 + j])
    for (var j=0; j<4; j++) vc.push(c[i*4 + j])
  }
  return new Float32Array(vc)
}

function loadImage(source, success, fail) {
  var image = new Image()
  image.crossOrigin = 'anonymous'
  image.src = source
  image.onload = function() {
    success(image)
  }
  image.onerror = function() {
    fail()
  }
}

function triangleNorm(p,q,r) {
  a = [q[0] - p[0], q[1] - p[1], q[2] - p[2]]
  b = [r[0] - p[0], r[1] - p[1], r[2] - p[2]]
  return [
    a[1] * b[2] - a[2] * b[1],
    a[2] * b[0] - a[0] * b[2],
    a[0] * b[1] - a[1] * b[0]
  ]
}

function randomDistribution(d) {
  var sum = 0
  var weights = Object.values(d)
  var choices = Object.keys(d)
  var n = weights.length

  for (var i=0; i<n; i++)
    sum += weights[i]

  var d_array = []
  for (var i=0; i<n; i++) {
    choice = choices[i]
    weight = d[choice]
    d_array.push([1*choice, weight / sum])
  }

  d_array.sort(function(a, b) {
    return a[1] > b[1]
  })

  var p = Math.random()

  var counter = 0
  for (var i=0; i<n; i++) {
    choice = d_array[i][0]
    weight = d_array[i][1]
    if (counter + weight > p)
      return choice

    counter += weight
  }
}
