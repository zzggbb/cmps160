var _inputHandler = null;
var drawing_mode = 'squares'
var color_mode = 'rainbow'
var mouse_down = false
var ambient_on = true
var projection_mode = 'perspective'
var shading_mode = 'normal'
var fog_on = true
var ground_clicked = false
var first_click = false

var velocity = { x: 0, y: 0, z: 0 }
var FOV = 60

var last_x
var last_y

var pitch = 0
var yaw = 0


const FOV_MIN = 15
const FOV_MAX = 90

class InputHandler {
  constructor(canvas, scene) {
    this.canvas = canvas;
    this.scene = scene;

    _inputHandler = this;

    // Mouse Events
    this.canvas.onmousedown = function(ev) {
      if (!first_click) {
        renderer.play()
        first_click = true
        setInterval(function() {
          FPS = renderer.frame_rate.toFixed(2)
        }, 100)
      }

      mouse_down = true;
      var xy = _inputHandler.getXY(ev)
      last_x = xy.x
      last_y = xy.y

      var cx = linscale(xy.x, -1, 1, 0, canvas.width)
      var cy = linscale(xy.y, -1, 1, 0, canvas.height)

      gl.uniform1i(gl.getUniformLocation(program, 'u_Clicked'), 1);
      renderer.render()
      var pixels = new Uint8Array(4)
      gl.readPixels(cx,cy,1,1,gl.RGBA,gl.UNSIGNED_BYTE,pixels)
      gl.uniform1i(gl.getUniformLocation(program, 'u_Clicked'), 0);

      ground_clicked = pixels[1] == 255

    }
    this.canvas.onmouseup = function() {
      mouse_down = false
    }
    this.canvas.onmousemove = function(ev) {
      var pos = _inputHandler.getXY(ev)

      if (pos.x > 0.97 || pos.x < -0.97 || pos.y > 0.97 || pos.y < -0.97)
        mouse_down = false

      if (mouse_down) {
        var dx = pos.x - last_x
        var dy = pos.y - last_y
        last_x = pos.x
        last_y = pos.y

        pitch += 100 * dy
        yaw += 100 * dx
      }
    }

    document.addEventListener('wheel', function(event) {
      var dy = event.deltaY / Math.abs(event.deltaY)
      FOV += dy
      if (FOV < FOV_MIN) FOV = FOV_MIN
      if (FOV > FOV_MAX) FOV = FOV_MAX
    })

    document.addEventListener('keydown', function(event) {
      switch (event.key) {
        case 'w': velocity.z = -1; break
        case 'a': velocity.x = -1; break
        case 's': velocity.z = 1; break
        case 'd': velocity.x = 1; break

        case 'f':
          fog_on = !fog_on
          gl.uniform1i(gl.getUniformLocation(program, 'u_FogOn'), fog_on? 1 : 0)
          break;

        case 'z':
          if (projection_mode == 'perspective')
            projection_mode = 'orthographic'
          else
            projection_mode = 'perspective'
          break

        case 'l':
          if (ambient_on) { // turn ambient off
            lighting.ambientOff()
            ambient_on = false
          } else { // turn ambient on
            lighting.ambientOn()
            ambient_on = true
          }
          break;

        case 'n':
          shading_mode = shading_mode == 'normal' ? 'phong' : 'normal'
          gl.uniform1i(gl.getUniformLocation(program, 'u_NormalShader'),
            shading_mode == 'normal' ? 1 : 0)
          break

        case 'p':
          if (renderer.paused)
            renderer.play()
          else
            renderer.pause()

      }
    })

    document.addEventListener('keyup', function(event) {
      switch (event.key) {
        case 'w': if (velocity.z == -1) velocity.z = 0; break
        case 'a': if (velocity.x == -1) velocity.x = 0; break
        case 's': if (velocity.z == 1) velocity.z = 0; break
        case 'd': if (velocity.x == 1) velocity.x = 0; break
      }
    }, false)

  }

  getXY(ev) {
    var x = ev.clientX
    var y = ev.clientY
    var rect = ev.target.getBoundingClientRect()
    var x = ((x - rect.x) - (rect.width / 2)) / (rect.width/2)
    var y = ((rect.height/2) - (y - rect.y)) / (rect.height/2)
    return {x: x, y: y}
  }
}
