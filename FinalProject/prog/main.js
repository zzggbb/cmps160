const TEX_VSHADER = `
precision mediump float;

uniform mat4 u_ProjMatrix;
uniform mat4 u_ViewMatrix;
uniform mat4 u_ModelMatrix;
uniform mat4 u_NormalMatrix;

attribute vec4 a_Position;
attribute vec4 a_Normal;
attribute vec2 a_TexCoord;

varying vec2 v_TexCoord;
varying vec3 v_Normal;
varying vec4 v_Position;
varying vec4 v_OtherPosition;

void main() {
  v_Position = u_ProjMatrix * u_ViewMatrix * u_ModelMatrix * a_Position;
  v_OtherPosition = u_ModelMatrix * a_Position;
  v_TexCoord = a_TexCoord;
  v_Normal = normalize(vec3(u_NormalMatrix * a_Normal));

  gl_Position = v_Position;
}`

const TEX_FSHADER = `
precision mediump float;

uniform sampler2D u_Sampler;
uniform vec4 u_LightColor;
uniform vec3 u_LightPosition;
uniform vec4 u_AmbientLight;
uniform vec4 u_EyePosition;

uniform int u_NormalShader;
uniform int u_Clicked;
uniform int u_FogOn;

varying vec2 v_TexCoord;
varying vec3 v_Normal;
varying vec4 v_Position;
varying vec4 v_OtherPosition;

void main() {
  vec3 normal = normalize(v_Normal);

  if (u_Clicked == 1) {
    gl_FragColor = vec4(abs(normal), 1.0);
    return;
  }

  vec3 lightDirection = normalize(u_LightPosition - v_Position.xyz);
  float NdotL = max(dot(lightDirection, normal), 0.0);
  vec3 textureColor = texture2D(u_Sampler, v_TexCoord).xyz;

  vec3 toEye = (u_EyePosition - v_Position).xyz;
  vec3 toEyeDir = normalize(toEye);

  vec3 reflectDirection = normalize(reflect(-lightDirection, normal));

  float strength = 0.5;
  float spec = pow(max(dot(toEyeDir, reflectDirection), 0.0), 8.0);
  vec3 specular = strength * spec * u_LightColor.rgb;

  vec3 diffuse = u_LightColor.rgb * NdotL;
  vec3 ambient = u_AmbientLight.rgb;

  vec3 color;
  if (u_NormalShader == 1)
    color = abs(normal);
  else
    color = textureColor * (diffuse + ambient + specular);

  float toEyeDist = distance(u_EyePosition.xyz, v_OtherPosition.xyz);
  vec3 fog_color = vec3(1.0, 1.0, 1.0);
  float fog_begin = 5.0;
  float fog_total = 30.0;
  float fog_factor = clamp(
    (fog_total - toEyeDist) / (fog_total - fog_begin),
    0.0,
    1.0
  );

  if (u_FogOn == 0)
    fog_factor = 1.0;

  color = mix(fog_color, color, fog_factor);
  gl_FragColor = vec4(color, 1.0);
}`

var gl, program, lighting, scene, renderer, inputHandler
var FPS = null
var canvas;
function main() {
  canvas = document.getElementById("webgl")
  gl = getWebGLContext(canvas)
  if (!gl) {
    console.log("Failed to get WebGL rendering context")
    return
  }

  gl.enable(gl.DEPTH_TEST)
  gl.enable(gl.POLYGON_OFFSET_FILL)

  program = createShaderProgram(gl, TEX_VSHADER, TEX_FSHADER)

  lighting = new Lighting(
    [0, 0, 1, 1],
    [0, 1, 0, 1]
  )

  lighting.ambientOn()
  gl.uniform1i(gl.getUniformLocation(program, 'u_NormalShader'), 1)
  gl.uniform1i(gl.getUniformLocation(program, 'u_Clicked'), 0)
  gl.uniform1i(gl.getUniformLocation(program, 'u_FogOn'), 1)

  scene = new Scene()
  inputHandler = new InputHandler(canvas, scene)
  renderer = new Renderer(scene, program)
  renderer.kickoff()

  hud_main()

  loadImage('./skybox.jpg', function(image) {
    scene.addGeometry(new TexCube(0,0,0,SIZE,image))
    loadImage('./rock.jpg', function(image) {
      scene.addGeometry(new Landscape(0,0,0,SIZE,SIZE,image))
    })
  })

}
