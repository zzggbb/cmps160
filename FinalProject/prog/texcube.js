class TexCube {
  constructor(x, y, z, size, image) {
    this.x = x
    this.y = y
    this.z = z
    this.size = size
    this.image = image
    this.vdata = new Float32Array([
      // back
      1,1,-1, .75,.5, 0,0,1,   1,-1,-1, .75,.25, 0,0,1,   -1,1,-1, 1,.5, 0,0,1,
      -1,1,-1, 1,.5, 0,0,1,    1,-1,-1, .75,.25, 0,0,1,   -1,-1,-1, 1,.25, 0,0,1,
      // front
      -1,1,1, .25,.5, 0,0,-1,    -1,-1,1, .25,.25, 0,0,-1,    1,-1,1, .5,.25, 0,0,-1,
      -1,1,1, .25,.5, 0,0,-1,    1,-1,1, .5,.25, 0,0,-1,      1,1,1, .5,.5, 0,0,-1,
      // top
      -1,1,-1, .25,.75, 0,-1,0,  1,1,1, .5,.5, 0,-1,0,       1,1,-1, .5, .75, 0,-1,0,
      -1,1,-1, .25,.75, 0,-1,0,  -1,1,1, .25, .5, 0,-1,0,    1,1,1,  .5,.5, 0,-1,0,
      // left
      -1,1,-1, 0,.5, 1,0,0,     -1,-1,-1, 0,.25, 1,0,0,    -1,-1,1, .25,.25, 1,0,0,
      -1,1,-1, 0,.5, 1,0,0,     -1,1,1, .25,.5, 1,0,0,     -1,-1,1, .25,.25, 1,0,0,
      // bottom
      -1,-1,1, .25,.25, 0,1,0,   -1,-1,-1, .25,0, 0,1,0,    1,-1,-1, .5,0, 0,1,0,
      -1,-1,1, .25,.25, 0,1,0,   1,-1,1, .5,.25, 0,1,0,      1,-1,-1, .5,0, 0,1,0,
      // right
      1,1,1, .5,.5, -1,0,0,       1,-1,1, .5,.25, -1,0,0,      1,-1,-1, .75,.25, -1,0,0,
      1,1,1, .5,.5, -1,0,0,       1,1,-1, .75,.5, -1,0,0,     1,-1,-1, .75,.25, -1,0,0
    ]);

    this.FSIZE = this.vdata.BYTES_PER_ELEMENT
    this.chunk_size = 8 // 3 vertices, 2 uv, 3 normal
    this.n = this.vdata.length/this.chunk_size;
    this.stride = this.chunk_size * this.FSIZE

    this.setupTexture()

    this.model_matrix = new Matrix4()
    this.model_matrix.setIdentity()
    this.model_matrix.translate(this.x, this.y, this.z)
    this.model_matrix.scale(this.size/2, this.size/2, this.size/2)
    this.model_matrix_loc = gl.getUniformLocation(program, 'u_ModelMatrix')

    this.normal_matrix = new Matrix4()
    this.normal_matrix.setInverseOf(this.model_matrix)
    this.normal_matrix.transpose()
    this.normal_matrix_loc = gl.getUniformLocation(program, 'u_NormalMatrix')
  }

  setupStuff(){
    this.vbuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.vbuffer);
    gl.bufferData(gl.ARRAY_BUFFER, this.vdata, gl.STATIC_DRAW);

    // source attribute 'a_Position' from vdata
    this.a_Position = gl.getAttribLocation(program, 'a_Position')
    gl.vertexAttribPointer(this.a_Position, 3, gl.FLOAT, false, this.stride, 0)
    gl.enableVertexAttribArray(this.a_Position)

    // source attribute 'a_TexCoord' from vdata
    var a_TexCoord = gl.getAttribLocation(program, 'a_TexCoord')
    gl.vertexAttribPointer(a_TexCoord, 2, gl.FLOAT, false, this.stride, this.FSIZE * 3)
    gl.enableVertexAttribArray(a_TexCoord)

    // source attribute 'a_Normal' from vdata
    var a_Normal = gl.getAttribLocation(program, 'a_Normal')
    gl.vertexAttribPointer(a_Normal, 3, gl.FLOAT, false, this.stride, this.FSIZE * 5)
    gl.enableVertexAttribArray(a_Normal)
  }

  setupTexture(){
    this.texture = gl.createTexture()
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1)
    gl.activeTexture(gl.TEXTURE0)
    gl.bindTexture(gl.TEXTURE_2D, this.texture)
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
    gl.uniform1i(gl.getUniformLocation(program, 'u_Sampler'), 0)
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, this.image)
  }

  render() {
    gl.bindTexture(gl.TEXTURE_2D, this.texture)

    this.setupStuff()

    gl.uniformMatrix4fv(this.model_matrix_loc, false, this.model_matrix.elements)
    gl.uniformMatrix4fv(this.normal_matrix_loc, false, this.normal_matrix.elements)
    gl.drawArrays(gl.TRIANGLES, 0, this.n)
  }
}
