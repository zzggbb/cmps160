function hud_main() {
  var hud = document.getElementById('hud')
  var context = hud.getContext('2d')

  // pass events down to the opengl canvas
  hud.onmousedown = inputHandler.canvas.onmousedown
  hud.onmouseup = inputHandler.canvas.onmouseup
  hud.onmousemove = inputHandler.canvas.onmousemove

  var tick = function() {
    draw_hud(context)
    requestAnimationFrame(tick, hud)
  }
  tick()
}

function draw_hud(ctx) {
  ctx.clearRect(0,0,800,800)
  font_size = 12
  line_spacing = 1.3
  ctx.font = `${font_size}px "Monospace"`
  ctx.fillStyle = 'rgba(0, 0, 0, 1)'

  if (!first_click) {
    text = `Welcome to the mountains!

The mountains are randomly generated each time the page is reloaded

Here are your controls:
- Fly around using keys 'w', 'a', 's', and 'd'
- Look around by clicking and dragging with the mouse
- Toggle between orthographic and perspective mode with the 'z' key
- Zoom in and out (while in perspective mode) with the mouse wheel
- Toggle between phong and normal shading with the 'n' key
- Toggle ambient lighting (while using phong shading) with the 'l' key
- Pause or play the renderer with the 'p' key
- Toggle the fog with the 'f' key

Please click anywhere to begin...

Made by Zane Bradley on May 19th, 2019

This is a final project for the Introduction to Computer Graphics
course at the University of California, Santa Cruz.

Lighting and 3D geometry is rendered with webGL. Text is rendered
using an HTML canvas.

Source code is available at:
  https://gitlab.com/zzggbb/cmps160/tree/master/FinalProject
    `
    splitted = text.split('\n')
    max_width = 0
    for (var i=0; i<splitted.length; i++) {
      text = splitted[i]
      width = ctx.measureText(text).width
      if (width > max_width)
        max_width = width
    }

    for (var i=0; i<splitted.length; i++)
      ctx.fillText(splitted[i],
        canvas.width/2 - max_width/2,
        canvas.height/2 + i*font_size*line_spacing)

    return
  }

  var pos = renderer.eye
  var x = pos.x.toFixed(2)
  var y = pos.y.toFixed(2)
  var z = pos.z.toFixed(2)
  var _pitch = ((pitch%360).toFixed(2) + '').padStart(7, ' ')
  var _yaw = ((yaw%360).toFixed(2) + '').padStart(7, ' ')

  ctx.fillText(`FPS: ${FPS}`, 40, 60)
  ctx.fillText(`FOV (scroll): ${FOV}`, 40, 80)
  ctx.fillText(`Projection Mode (z): ${projection_mode}`, 40, 100)
  ctx.fillText(`Shading Mode (n): ${shading_mode}`, 40, 120)
  ctx.fillText(`Ambient Lighting (l): ${ambient_on ? 'on' : 'off'}`, 40, 140)
  ctx.fillText(`Render State (p): ${renderer.paused ? 'paused' : 'running'}`, 40, 160)
  ctx.fillText(`Position (w,a,s,d): x=${x} y=${y} z=${z}`, 40, 180)
  ctx.fillText(`Orientation (mouse): pitch=${_pitch} yaw=${_yaw}`, 40, 200)
  ctx.fillText(`Fog (f): ${fog_on}`, 40, 220)

  ground_message = ground_clicked ?
    'CONGRATS! YOU CLICKED THE GROUND!' :
    "I see you haven't clicked on the ground..."
  ctx.fillText(ground_message, 40, 240)
}
