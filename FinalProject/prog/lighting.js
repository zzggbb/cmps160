class Lighting {
  constructor(ambient_color, position_color) {
    this.ambient_color = ambient_color
    this.ambient_loc = gl.getUniformLocation(program, 'u_AmbientLight')

    this.position_color = position_color
    this.position = new Vector3([0, 10, 20])

    this.velocity = 10 // degrees/sec
    this.position_loc = gl.getUniformLocation(program, 'u_LightPosition')
    this.position_color_loc = gl.getUniformLocation(program, 'u_LightColor')

    gl.uniform4fv(this.position_color_loc, this.position_color)
  }

  update(dt) {
    var rotate = new Matrix4()
    var dtheta = this.velocity * dt
    rotate.setRotate(100 * dt, 0, 1, 0)
    var p = this.position.elements
    this.position = rotate.multiplyVector3(new Vector3([p[0], p[1], p[2]]))

    gl.uniform3fv(this.position_loc, this.position.elements)
  }

  ambientOn() {
    gl.uniform4fv(this.ambient_loc, this.ambient_color);
  }

  ambientOff() {
    gl.uniform4fv(this.ambient_loc, [0, 0, 0, 1]);
  }
}
