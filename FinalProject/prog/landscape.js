class Landscape {
  constructor(x, y, z, length, width, image) {
    this.x = x
    this.y = y
    this.z = z
    this.length = length
    this.width = width
    this.image = image

    // height: probability weighting of that height
    var height_distribution = { 0: 100, 1: 30, 2: 10, 3: 5, 4: 3, 5: 1 }

    this.height_map = []
    for (var i=0; i<length; i++) {
      this.height_map[i] = []
      for (var j=0; j<width; j++) {
        this.height_map[i][j] = randomDistribution(height_distribution)
      }
    }

    this.vdata = []

    for (var a = 0; a < width - 1; a++) {
      for (var b = 0; b < length - 1; b++) {
        var top1 = [a, this.height_map[a][b], b]
        var top2 = [a, this.height_map[a][b+1], b+1]
        var top3 = [a+1, this.height_map[a+1][b], b]
        var norm_top = triangleNorm(top1, top2, top3)

        var bot1 = top2
        var bot2 = [a+1, this.height_map[a+1][b+1], b+1]
        var bot3 = top3
        var norm_bot = triangleNorm(bot1, bot2, bot3)

        this.vdata = this.vdata.concat([
          top1[0], top1[1], top1[2], 0,1, norm_top[0], norm_top[1], norm_top[2],
          top2[0], top2[1], top2[2], 0,0, norm_top[0], norm_top[1], norm_top[2],
          top3[0], top3[1], top3[2], 1,1, norm_top[0], norm_top[1], norm_top[2],

          bot1[0], bot1[1], bot1[2], 0,1, norm_bot[0], norm_bot[1], norm_bot[2],
          bot2[0], bot2[1], bot2[2], 0,0, norm_bot[0], norm_bot[1], norm_bot[2],
          bot3[0], bot3[1], bot3[2], 1,1, norm_bot[0], norm_bot[1], norm_bot[2],

        ])
      }
    }

    this.vdata = new Float32Array(this.vdata)

    this.FSIZE = this.vdata.BYTES_PER_ELEMENT
    this.chunk_size = 8 // 3 vertices, 2 uv, 3 normal
    this.n_triangles = (length-1) * (width-1) * 2
    this.n = this.n_triangles * 3
    this.stride = this.chunk_size * this.FSIZE

    this.setupTexture()

    // center at the origin
    this.model_matrix = new Matrix4()
    this.model_matrix.setTranslate(x - this.width/2, y, z - this.length/2)
    this.model_matrix_loc = gl.getUniformLocation(program, 'u_ModelMatrix')

    this.normal_matrix = new Matrix4()
    this.normal_matrix.setInverseOf(this.model_matrix)
    this.normal_matrix.transpose()
    this.normal_matrix_loc = gl.getUniformLocation(program, 'u_NormalMatrix')
  }

  setupStuff(){
    this.vbuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.vbuffer);
    gl.bufferData(gl.ARRAY_BUFFER, this.vdata, gl.STATIC_DRAW);

    // source attribute 'a_Position' from vdata
    this.a_Position = gl.getAttribLocation(program, 'a_Position')
    gl.vertexAttribPointer(this.a_Position, 3, gl.FLOAT, false, this.stride, 0)
    gl.enableVertexAttribArray(this.a_Position)

    // source attribute 'a_TexCoord' from vdata
    var a_TexCoord = gl.getAttribLocation(program, 'a_TexCoord')
    gl.vertexAttribPointer(a_TexCoord, 2, gl.FLOAT, false, this.stride, this.FSIZE * 3)
    gl.enableVertexAttribArray(a_TexCoord)

    // source attribute 'a_Normal' from vdata
    var a_Normal = gl.getAttribLocation(program, 'a_Normal')
    gl.vertexAttribPointer(a_Normal, 3, gl.FLOAT, false, this.stride, this.FSIZE * 5)
    gl.enableVertexAttribArray(a_Normal)
  }

  setupTexture(){
    // setup texture
    this.texture = gl.createTexture()
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1)
    gl.activeTexture(gl.TEXTURE0)
    gl.bindTexture(gl.TEXTURE_2D, this.texture)
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
    gl.uniform1i(gl.getUniformLocation(program, 'u_Sampler'), 0)
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, this.image)
  }

  render() {
    gl.bindTexture(gl.TEXTURE_2D, this.texture)

    this.setupStuff()

    gl.uniformMatrix4fv(this.model_matrix_loc, false, this.model_matrix.elements)
    gl.uniformMatrix4fv(this.normal_matrix_loc, false, this.normal_matrix.elements)
    gl.drawArrays(gl.TRIANGLES, 0, this.n)
  }
}
