class Scene {
  constructor() {
    this.geometries = [] // Geometries being drawn on canvas
  }

  addGeometry(geometry) {
    this.geometries.push(geometry)
  }

  clearGeometry() {
    this.geometries = []
    this.render()
  }

  render() {
    for (var i = 0; i < this.geometries.length; i++)
      this.geometries[i].render()
  }
}
