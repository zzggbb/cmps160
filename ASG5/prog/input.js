var _inputHandler = null;
var drawing_mode = 'squares'
var color_mode = 'rainbow'
var mouse_down = false
var ambient_on = true
var projection_mode = 'perspective'
var shading_mode = 'phong'

var velocity = { x: 0, y: 0, z: 0 }
var fov = 60

var last_x
var last_y

var pitch = 0
var yaw = 0

const FOV_MIN = 15
const FOV_MAX = 120

class InputHandler {
  constructor(canvas, scene) {
    this.canvas = canvas;
    this.scene = scene;

    _inputHandler = this;

    document.getElementById('fov').innerHTML = fov
    document.getElementById('projection').innerHTML = projection_mode

    // Mouse Events
    this.canvas.onmousedown = function(ev) {
      mouse_down = true;
      var xy = _inputHandler.getXY(ev)
      last_x = xy.x
      last_y = xy.y
    }
    this.canvas.onmouseup = function() {
      mouse_down = false
    }
    this.canvas.onmousemove = function(ev) {
      var pos = _inputHandler.getXY(ev)

      if (pos.x > 0.97 || pos.x < -0.97 || pos.y > 0.97 || pos.y < -0.97)
        mouse_down = false

      if (mouse_down) {
        var dx = pos.x - last_x
        var dy = pos.y - last_y
        last_x = pos.x
        last_y = pos.y

        pitch += 100 * dy
        yaw += 100 * dx
      }
    }

    document.addEventListener('wheel', function(event) {
      var dy = event.deltaY / Math.abs(event.deltaY)
      fov += dy
      if (fov < FOV_MIN) fov = FOV_MIN
      if (fov > FOV_MAX) fov = FOV_MAX
      document.getElementById('fov').innerHTML = fov
    })

    document.addEventListener('keydown', function(event) {
      switch (event.key) {
        case 'w': velocity.z = -1; break
        case 'a': velocity.x = -1; break
        case 's': velocity.z = 1; break
        case 'd': velocity.x = 1; break

        case 'z':
          if (projection_mode == 'perspective') projection_mode = 'orthographic'
          else projection_mode = 'perspective'
          document.getElementById('projection').innerHTML = projection_mode
          break

        case 'n':
          shading_mode = shading_mode == 'normal' ? 'phong' : 'normal'

          gl.uniform1f(gl.getUniformLocation(program, 'u_NormalShader'),
            shading_mode == 'normal' ? 1.0 : 0.0)

          document.getElementById('shading_mode').innerHTML = shading_mode
          break

      }
    })

    document.addEventListener('keyup', function(event) {
      switch (event.key) {
        case 'w': if (velocity.z == -1) velocity.z = 0; break
        case 'a': if (velocity.x == -1) velocity.x = 0; break
        case 's': if (velocity.z == 1) velocity.z = 0; break
        case 'd': if (velocity.x == 1) velocity.x = 0; break
      }
    }, false)

    var toggle_ambient = document.getElementById('toggle_ambient')
    toggle_ambient.onclick = function(){
      if (ambient_on) { // turn ambient off
        lighting.ambientOff()
        ambient_on = false
        toggle_ambient.innerHTML = 'Enable Ambient Light'
      } else { // turn ambient on
        lighting.ambientOn()
        ambient_on = true
        toggle_ambient.innerHTML = 'Disable Ambient Light'
      }
    }

    document.getElementById('playpause').onclick = function() {
      if (renderer.paused) {
        renderer.play()
        document.getElementById('playpause').innerHTML = 'Pause Rendering'
      } else {
        renderer.pause()
        document.getElementById('playpause').innerHTML = 'Resume Rendering'
      }
    }
  }

  getXY(ev) {
    var x = ev.clientX
    var y = ev.clientY
    var rect = ev.target.getBoundingClientRect()
    var x = ((x - rect.x) - (rect.width / 2)) / (rect.width/2)
    var y = ((rect.height/2) - (y - rect.y)) / (rect.height/2)
    return {x: x, y: y}
  }
}
