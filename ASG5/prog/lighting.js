class Lighting {
  constructor(ambient_color, position_color) {
    this.ambient_color = ambient_color
    this.ambient_loc = gl.getUniformLocation(program, 'u_AmbientLight')

    this.position_color = position_color
    this.position = [30, 10, 30]
    this.velocity = 10 // units/sec
    this.position_loc = gl.getUniformLocation(program, 'u_LightPosition')
    this.position_color_loc = gl.getUniformLocation(program, 'u_LightColor')
    gl.uniform4fv(this.position_color_loc, this.position_color)
  }

  update(dt) {
    var x = this.position[0]
    if (x < -30 || x > 30)
      this.velocity *= -1;
    this.position[0] += this.velocity * dt/1000;

    gl.uniform3fv(this.position_loc, this.position)
  }

  ambientOn() {
    gl.uniform4fv(this.ambient_loc, this.ambient_color);
  }

  ambientOff() {
    gl.uniform4fv(this.ambient_loc, [0, 0, 0, 1]);
  }
}
