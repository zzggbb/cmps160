class Geometry {
  constructor(shader) {
    this.vertices = []; // Vertex objects
    this.color = [];    // array[4]
    this.shader = shader
    this.modelMatrix = new Matrix4();
    this.origin = new Float32Array([0, 0, 0, 0]);
    this.color = new Float32Array([1, 0, 0, 1]);
  }
  update(){
    return
  }
  toString() {
    var out = `Geometry(color=[${this.color}] vertices:\n`
    for (var i = 0; i < this.vertices.length; i++)
      out += this.vertices[i].toString() + '\n'
    out += ')'
    return out
  }
}
