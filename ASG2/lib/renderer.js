var _renderer = null;
class Renderer {
  constructor(gl, scene, camera) {
    this.gl = gl;
    this.scene = scene;
    this.camera = camera;
    this.initGLSLBuffers();
    // Setting canvas' clear color
    this.gl.clearColor(0.0, 0.0, 0.0, 1.0);
    // Use the z-buffer when drawing
    this.gl.enable(gl.DEPTH_TEST);
    _renderer = this;
  }
  start() {
    _renderer.render();
    requestAnimationFrame(_renderer.start);
  }
  render() {
    // Clear the geometry onscreen
    this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
    for (var i = 0; i < this.scene.geometries.length; i++) {
      var geometry = this.scene.geometries[i];
      // Switch to the geometry's shader
      var program = geometry.shader.program
      this.gl.useProgram(program)
      this.gl.program = program

      geometry.update();

      var data_indices_dataCounts = interleaveVertexData(geometry.vertices)
      var data = data_indices_dataCounts[0]
      var indices = data_indices_dataCounts[1]

      if (geometry.indices)
        indices = geometry.indices

      indices = new Uint16Array(indices)

      var dataCounts = data_indices_dataCounts[2]

      // send attribute position to vertex shader
      this.gl.bufferData(this.gl.ARRAY_BUFFER, data, this.gl.STATIC_DRAW);
      var a_Position = this.gl.getAttribLocation(shader.program, 'a_Position')
      this.gl.vertexAttribPointer(a_Position, 3, gl.FLOAT, false, 0, 0)
      this.gl.enableVertexAttribArray(a_Position)

      // send uniform origin to vertex shader
      this.gl.uniform4fv(
        this.gl.getUniformLocation(shader.program, 'u_Origin'),
        geometry.origin
      )

      // send uniform color to fragment shader
      this.gl.uniform4fv(
        this.gl.getUniformLocation(shader.program, 'u_FragColor'),
        geometry.color
      )

      // send uniform model matrix to vertex shader
      var matrix = new Float32Array(geometry.getModelMatrix())
      this.gl.uniformMatrix4fv(
        this.gl.getUniformLocation(shader.program, 'u_ModelMatrix'),
        false,
        matrix
      )

      this.gl.bufferData(this.gl.ELEMENT_ARRAY_BUFFER, indices, this.gl.STATIC_DRAW);
      this.gl.drawElements(this.gl.TRIANGLES, indices.length, this.gl.UNSIGNED_SHORT, 0)
    }
  }

  // creates and binds an array buffer and element array buffer
  initGLSLBuffers() {
    var attributeBuffer = this.gl.createBuffer();
    var indexBuffer = this.gl.createBuffer();
    if (!attributeBuffer || !indexBuffer) {
        console.error("Failed to create buffers!");
        return;
    }
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, attributeBuffer);
    this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
  }
}
