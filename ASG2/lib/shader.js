class Shader {
   /* @param gl WebGL instance
    * @param {String} vShader Vertex Shader used for shading program
    * @param {String} fShader Fragment Shader used for shading program
    * @returns {Shader} Shader created */
    constructor(gl, vShader, fShader) {
        this.gl = gl;

        this.program = createShaderProgram(this.gl, vShader, fShader)
        this.uniformLocations = [];
        this.attributeLocations = [];
    }

    /* Adds an attribute variable and stores it's location.
     * Only name is necessary since an attribute's information is stored by a
     * geometry's vertices.
     * @param {String} attributeName Name of the attribute variable */
    addAttribute(attributeName) {
      var loc = this.gl.getAttribLocation(this.program, attributeName)
      if (loc < 0) {
        console.error(
          "Couldn't get location of '" + attributeName +
          "'. Did you forget to declare and use it in a shader?"
        )
        return
      }
      this.attributeLocations.push({
        "name": attributeName,
        "location": loc
      });
    }

    /* Adds a uniform variable, stores it's type, and stores it's location.
     * Acceptable types: float, vec2, vec3, vec4, mat2, mat3, mat4
     * @param {String} uniformName Name of the uniform variable
     * @param {String} type Type of the uniform variable
     * @param value Value assigned to uniform variable */
    addUniform(uniformName, type, value) {
      var loc = this.gl.getUniformLocation(this.program, uniformName)
      if (loc < 0) {
        console.error(
          "Couldn't get location of '" + uniformName+
          "'. Did you forget to declare and use it in a shader?"
        )
        return
      }
      this.uniformLocations.push({
        "name": uniformName,
        "type": type,
        "location": loc,
        "value": value
      });
    }
}
