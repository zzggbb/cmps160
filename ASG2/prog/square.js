class Square extends Geometry {
  constructor(shader, color, size, centerX, centerY) {
    const EDGES = 4
    var vertices = getRegularPolygonVertices(EDGES, size, centerX, centerY)
    super(shader, vertices, color)
    this.shader = shader
    this.color = color
    this.vertices = vertices
    this.origin = [centerX, centerY, 0, 0]

    this.angle = 0
    this.time_last = Date.now()
    this.step = 30
  }
  getModelMatrix() {
    var rad = this.angle * Math.PI / 180
    return [
      Math.cos(rad),  Math.sin(rad), 0, 0,
      -Math.sin(rad), Math.cos(rad), 0, 0,
      0,                     0,                    1, 0,
      0,                     0,                    0, 1
    ]
  }
  update() {
    var time_now = Date.now()
    var elapsed_seconds = (time_now - this.time_last) / 1000
    this.time_last = time_now
    this.angle = animate_angle(this.angle, this.step, elapsed_seconds)
  }
}
