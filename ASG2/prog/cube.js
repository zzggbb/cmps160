class Cube extends Geometry {
  constructor(shader, color, size, centerX, centerY) {
    var vertices = [
      new Vertex([centerX - size/2, centerY - size/2, -size/2], [], []),
      new Vertex([centerX - size/2, centerY - size/2, size/2], [], []),
      new Vertex([centerX - size/2, centerY + size/2, -size/2], [], []),
      new Vertex([centerX - size/2, centerY + size/2, size/2], [], []),
      new Vertex([centerX + size/2, centerY - size/2, -size/2], [], []),
      new Vertex([centerX + size/2, centerY - size/2, size/2], [], []),
      new Vertex([centerX + size/2, centerY + size/2, -size/2], [], []),
      new Vertex([centerX + size/2, centerY + size/2, size/2], [], [])
    ]
    super(shader, vertices, color)

    this.indices = new Uint16Array([
      7, 3, 1, 5,
      3, 2, 0, 1,
      7, 6, 2, 3,
      4, 0, 2, 6,
      7, 6, 4, 5,
      5, 4, 0, 1
    ])

    this.shader = shader
    this.color = color
    this.vertices = vertices
    this.origin = [centerX, centerY, 0, 0]

    this.angle = 0
    this.time_last = Date.now()
    this.step = 30
  }
  getModelMatrix() {
    var m = new Matrix4()
    m.setRotate(this.angle, 0, 1, 1)
    return m.elements
  }
  update() {
    var time_now = Date.now()
    var elapsed_seconds = (time_now - this.time_last) / 1000
    this.time_last = time_now
    this.angle = animate_angle(this.angle, this.step, elapsed_seconds)
  }
}
