const VSHADER = `
  // attributes can only be defined in the vertex shader
  precision mediump float;
  attribute vec4 a_Position;
  uniform vec4 u_Origin;
  uniform mat4 u_ModelMatrix;
  void main() {
    gl_Position = u_ModelMatrix * (a_Position - u_Origin) + u_Origin;
  }
`;

const FSHADER = `
  // attributes cannot be defined in a fragment shader
  precision mediump float;
  uniform vec4 u_FragColor;
  void main() {
    gl_FragColor = u_FragColor;
  }
`;

var gl, shader, scene, renderer, camera, inputHandler
function main() {
  var canvas = document.getElementById("webgl")
  gl = getWebGLContext(canvas)
  if (!gl) {
    console.log("Failed to get WebGL rendering context")
    return
  }
  scene = new Scene()
  inputHandler = new InputHandler(canvas, scene)
  shader = new Shader(gl, VSHADER, FSHADER)
  camera = null
  renderer = new Renderer(gl, scene, camera)
  renderer.start()
}
