var _inputHandler = null;
var drawing_mode = 'squares'
var mouse_down = false

class InputHandler {
  constructor(canvas, scene) {
    this.canvas = canvas;
    this.scene = scene;

    _inputHandler = this;

    // Mouse Events
    this.canvas.onmousedown = function(ev) { mouse_down = true; _inputHandler.click(ev) }
    this.canvas.onmouseup = function() { mouse_down = false }
    this.canvas.onmousemove = function(ev) { if (mouse_down) _inputHandler.click(ev) }

    document.getElementById('add_obj').onclick = function() {
      //var filename = document.getElementById('obj_file').value
      var filename = '/home/zane/school/cs160/ASG2/teapot.obj'

      //console.log(filename)

      if (!filename) {
        document.getElementById('error').innerHTML = "error: please choose a file first"
        return
      }

      document.getElementById('error').innerHTML = ''

      loadFile(filename, function(filestring) {
        scene.addGeometry(new LoadedOBJ(shader, filestring))
      })
    }

    document.getElementById('clear').onclick = function() { scene.clearGeometry() }
    document.getElementById('render').onclick = function() { renderer.render() }
    document.getElementById('squares').onclick = function() { drawing_mode = 'squares' }
    document.getElementById('triangles').onclick = function() { drawing_mode = 'triangles' }
    document.getElementById('circles').onclick = function() { drawing_mode = 'circles' }
    document.getElementById('cubes').onclick = function() { drawing_mode = 'cubes' }
  }

  getActiveColor() {
    return [document.getElementById('red_color').value,
    document.getElementById('green_color').value,
    document.getElementById('blue_color').value,
    1.0]
  }

  getActiveSize(){
    return 1 * document.getElementById('shape_size').value
  }

  getActiveSegments(){
    return 1 * document.getElementById('segments_input').value
  }

  getShape(x, y) {
    var color = this.getActiveColor()
    var size = this.getActiveSize()
    switch (drawing_mode) {
      case 'squares': return new Square(shader, color, size, x, y)
      case 'triangles': return new Triangle(shader, color, size, x, y)
      case 'circles': return new Circle(shader, color, size, this.getActiveSegments(), x, y)
      case 'cubes': return new Cube(shader, color, size, x, y)
    }
  }

  click(ev) {
    var x = ev.clientX
    var y = ev.clientY
    var rect = ev.target.getBoundingClientRect()
    var x = ((x - rect.x) - (rect.width / 2)) / (rect.width/2)
    var y = ((rect.height/2) - (y - rect.y)) / (rect.height/2)

    var size = 0.2

    console.log("click: ", x, y);
    var shape = this.getShape(x, y)
    this.scene.addGeometry(shape);
  }
}
