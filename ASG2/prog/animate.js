function animate_scale(scale, velocity, min, max, elapsed) {
  if (scale + velocity > max || scale + velocity < min) {
    velocity = -velocity
  }
  return velocity
}

function animate_angle(current_angle, step, time_elapsed) {
  return (current_angle + step * time_elapsed) % 360
}
