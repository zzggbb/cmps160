function getRegularPolygonVertices(edges, size) {
  var vertices = []
  for (var i = 0; i < edges; i++) {
    var angle = 2 * Math.PI * i / edges
    vertices.push(new Vertex(
      [Math.cos(angle) * size, Math.sin(angle) * size, 0],
      [], // no uv
      [], // no normal
    ))
  }
  return vertices
}
