class Shader {
   /* @param gl WebGL instance
    * @param {String} vShader Vertex Shader used for shading program
    * @param {String} fShader Fragment Shader used for shading program
    * @returns {Shader} Shader created */
    constructor(gl, vShader, fShader) {
        this.gl = gl;

        this.program = createShaderProgram(this.gl, vShader, fShader)
        this.uniformLocations = [];
        this.attributeLocations = [];
    }
}
