var _renderer = null;
class Renderer {
  constructor(scene, camera) {
    this.scene = scene;
    this.camera = camera;
    this.initGLSLBuffers();
    // Setting canvas' clear color
    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    // Use the z-buffer when drawing
    gl.enable(gl.DEPTH_TEST);
    _renderer = this;
    this.paused = true
  }
  kickoff() {
    _renderer.render();
    requestAnimationFrame(_renderer.kickoff);
  }
  play() {
    this.paused = false
  }
  pause() {
    this.paused = true
  }
  render() {
    if (this.paused) return
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    for (var i = 0; i < this.scene.geometries.length; i++) {
      this.scene.geometries[i].render()
    }
  }
  // creates and binds an array buffer and element array buffer
  initGLSLBuffers() {
    var attributeBuffer = gl.createBuffer();
    var indexBuffer = gl.createBuffer();
    if (!attributeBuffer || !indexBuffer) {
        console.error("Failed to create buffers!");
        return;
    }
    gl.bindBuffer(gl.ARRAY_BUFFER, attributeBuffer);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
  }
}
