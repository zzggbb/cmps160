class Vertex {
  constructor(point, uv, normal) {
    this.point = point // should be array[3] for 3d vertex
    this.uv = uv
    this.normal = normal
  }
  toString() {
    return `Vertex(point=[${this.point}] uv=[${this.uv}] norm=[${this.normal}])`
  }
}
