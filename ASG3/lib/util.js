function mergeVertsColors(v, c) {
  var vc = []
  for (var i=0; i<v.length/3; i++) {
    for (var j=0; j<3; j++) vc.push(v[i*3 + j])
    for (var j=0; j<4; j++) vc.push(c[i*4 + j])
  }
  return new Float32Array(vc)
}

function loadImage(source, success, fail) {
  var image = new Image()
  image.crossOrigin = 'anonymous'
  image.src = source
  image.onload = function() {
    success(image)
  }
  image.onerror = function() {
    fail()
  }
}

function loadTexture(n, texture, sampler, image) {
  gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1); // Flip the image's y axis
  // Enable texture unit0
  gl.activeTexture(gl.TEXTURE0);
  // Bind the texture object to the target
  gl.bindTexture(gl.TEXTURE_2D, texture);

  // Set the texture parameters
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);

  // Set the texture image
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, image);

  // Set the texture unit 0 to the sampler
  gl.uniform1i(sampler, 0);
}
