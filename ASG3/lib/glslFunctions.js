/**
 * Sends data to an attribute variable using a buffer.
 *
 * @private
 * @param {Float32Array} data Data being sent to attribute variable
 * @param {Number} dataCount The amount of data to pass per vertex
 * @param {String} attribName The name of the attribute variable
 */
function sendAttributeBufferToGLSL(data, dataCount, attribName) {
  // Recommendations: This piece of code should do these three things:
  // 1. Create a an attribute buffer
  // 2. Bind data to that buffer
  // 3. Enable the buffer for use
  //
  // Some modifications can be made to this function to improve performance. Ask
  // a TA in lab if you're interested in these modifications.

  var data = new Float32Array(data)
  var buffer = gl.createBuffer()
  if (!buffer) {
    console.error("gl.createBuffer() failed")
    return
  }
  gl.bindBuffer(gl.ARRAY_BUFFER, buffer)
  gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW)
  var loc = gl.getAttribLocation(gl.program, attribName)
  if (loc < 0) {
    console.error("gl.getAttribLocation(gl.program, " + attribName + ") failed")
    return
  }

  var normalized = false
  var stride = 0
  var pointer_offset = 0

  gl.vertexAttribPointer(
    loc, dataCount, gl.FLOAT,
    normalized, stride, pointer_offset)

  gl.enableVertexAttribArray(loc)
}

/**
 * Sends a float value to the specified uniform variable within GLSL shaders.
 * Prints an error message if unsuccessful.
 *
 * @param {float} val The float value being passed to uniform variable
 * @param {String} uniformName The name of the uniform variable
 */
function sendUniformFloatToGLSL(val, uniformName) {
  var loc = gl.getUniformLocation(gl.program, uniformName)
  gl.uniform1f(loc, val)
}

/**
 * Sends an JavaSript array (vector) to the specified uniform variable within
 * GLSL shaders. Array can be of length 2-4.
 *
 * @param {Array} val Array (vector) being passed to uniform variable
 * @param {String} uniformName The name of the uniform variable
 */
function sendUniformVec4ToGLSL(val, uniformName) {
  var loc = gl.getUniformLocation(gl.program, uniformName)
  gl.uniform4f(loc, val[0], val[1], val[2], val[3])
}
