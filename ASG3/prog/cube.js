class Cube extends Geometry {
  constructor(shader, colors, size, x, y) {
    var vertices = [
      new Vertex([-1, -1, -1], [], []),
      new Vertex([-1, -1, 1], [], []),
      new Vertex([-1,  1, -1], [], []),
      new Vertex([-1,  1, 1], [], []),
      new Vertex([1,  -1, -1], [], []),
      new Vertex([1,  -1, 1], [], []),
      new Vertex([1,   1, -1], [], []),
      new Vertex([1,   1, 1], [], [])
    ]
    super(shader, vertices, colors, x, y)

    this.size = size
    this.indices = new Uint16Array([
      7,3,1,  7,1,5, // front face
      7,5,4,  4,7,6, // right face
      4,5,0,  5,0,1, // bottom face
      3,1,0,  0,3,2, // left face
      7,3,2,  7,2,6, // top face
      4,6,2,  4,2,0  // back face
    ])

    this.angle = 0
    this.time_last = Date.now()
    this.step = 30
  }
  getModelMatrix() {
    var m = new Matrix4()
    m.setIdentity()
    m.scale(this.size, this.size, this.size)
    m.rotate(this.angle, 1, 1, 0)
    return m.elements
  }
  update() {
    var time_now = Date.now()
    var elapsed_seconds = (time_now - this.time_last) / 1000
    this.time_last = time_now
    this.angle = animate_angle(this.angle, this.step, elapsed_seconds)
  }
}
