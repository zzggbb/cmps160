class Circle extends Geometry {
  constructor(shader, colors, radius, segments, x, y) {
    var vertices = getRegularPolygonVertices(segments, radius)
    super(shader, vertices, colors, x, y)

    this.mode = gl.TRIANGLE_FAN

    this.tx = 0
    this.ty = 0

    this.time_last = Date.now()
    this.dtx = (Math.random() - 0.5) / 100
    this.dty = (Math.random() - 0.5) / 100
  }
  getModelMatrix() {
    return [
      1,       0,       0,       0,
      0,       1,       0,       0,
      0,       0,       1,       0,
      this.tx, this.ty, 0,       1
    ]
  }
  update() {
    var time_now = Date.now()
    var elapsed_seconds = (time_now - this.time_last) / 1000
    this.time_last = time_now
    this.tx += (this.dtx * elapsed_seconds)
    this.ty += (this.dty * elapsed_seconds)

    this.dtx += (Math.random() - 0.5) / 10
    this.dty += (Math.random() - 0.5) / 10
  }
}
