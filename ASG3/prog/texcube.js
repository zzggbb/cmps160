class TexCube {
  constructor(x, y, size, image) {
    this.angle = 0
    this.time_last = Date.now()
    this.step = 30
    this.n = 36;
    this.image = image
    this.origin = [x, y, 0, 0]
    this.size = size
    this.verticesTexCoords = new Float32Array([
      // back
      1,1,-1,0,3,    1,-1,-1,0,0,   -1,1,-1,3,3,
      -1,1,-1,3,3,   1,-1,-1,0,0,   -1,-1,-1,3,0,
      // front
      -1,1,1, 0,1,   -1,-1,1,0,0,   1,-1,1,2,0,
      -1,1,1, 0,1,    1,-1,1,2,0,   1,1,1,2,1,
      // top
      -1,1,-1, 0,1,   1,1,1, 1,0,   1,1,-1, 1,1,
      -1,1,-1, 0,1,   -1,1,1,0,0,   1,1,1,1,0,
      // left
      -1,1,-1,0,1,    -1,-1,-1,0,0.5,   -1,-1,1,1,0.5,
      -1,1,-1,0,1,    -1,1,1,1,1,       -1,-1,1,1,0.5,
      // bottom
      -1,-1,1,0,0,    -1,-1,-1,0,1,     1,-1,-1,1,1,
      -1,-1,1,0,0,    1,-1,1,1,0,       1,-1,-1,1,1,
      // right
      1,1,1,0,0.5,    1,-1,1,0,0,       1,-1,-1,1,0,
      1,1,1,0,0.5,    1,1,-1,1,0.5,     1,-1,-1,1,0,
    ]);
  }

  getModelMatrix() {
    var m = new Matrix4()
    m.setIdentity()
    m.scale(this.size, this.size, this.size)
    m.rotate(this.angle, 1, 1, 0)
    return m.elements
  }

  update() {
    var time = Date.now()
    var ela = (time - this.time_last) / 1000
    this.time_last = time
    this.angle = animate_angle(this.angle, this.step, ela)
  }

  render() {
    this.update()

    var program = createShaderProgram(gl, TEX_VSHADER, TEX_FSHADER)

    loadTexture(this.n, gl.createTexture(), gl.getUniformLocation(program, 'u_Sampler'), this.image)

    gl.uniform4fv(
      gl.getUniformLocation(program, 'u_Origin'),
      new Float32Array(this.origin)
    )

    gl.uniformMatrix4fv(
      gl.getUniformLocation(program, 'u_ModelMatrix'),
      false, new Float32Array(this.getModelMatrix())
    )

    var vertexTexCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vertexTexCoordBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, this.verticesTexCoords, gl.STATIC_DRAW);

    var FSIZE = this.verticesTexCoords.BYTES_PER_ELEMENT;
    var a_Position = gl.getAttribLocation(program, 'a_Position');
    gl.vertexAttribPointer(a_Position, 3, gl.FLOAT, false, FSIZE * 5, 0);
    gl.enableVertexAttribArray(a_Position);

    var a_TexCoord = gl.getAttribLocation(program, 'a_TexCoord');
    gl.vertexAttribPointer(a_TexCoord, 2, gl.FLOAT, false, FSIZE * 5, FSIZE * 3);
    gl.enableVertexAttribArray(a_TexCoord);

    gl.enable(gl.DEPTH_TEST)
    gl.enable(gl.POLYGON_OFFSET_FILL)

    gl.drawArrays(gl.TRIANGLES, 0, 36)
  }
}
