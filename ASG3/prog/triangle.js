class Triangle extends Geometry {
  constructor(shader, colors, size, x, y) {
    const EDGES = 3
    var vertices = getRegularPolygonVertices(EDGES, size)
    super(shader, vertices, colors, x, y)

    this.scale = 1
    this.time_last = Date.now()
    this.velocity = 0.01
    this.min = 1/10
    this.max = 1
  }
  getModelMatrix() {
    return [
      this.scale, 0,          0,          0,
      0,          this.scale, 0,          0,
      0,          0,          this.scale, 0,
      0,          0,          0,          1
    ]
  }
  update() {
    var time_now = Date.now()
    var elapsed_seconds = (time_now - this.time_last) / 1000
    this.time_last = time_now
    this.velocity = animate_scale(this.scale, this.velocity, this.min, this.max, elapsed_seconds)
    this.scale += this.velocity
  }
}
