const VSHADER = `
  // attributes can only be defined in the vertex shader
  precision mediump float;
  attribute vec4 a_Position;
  uniform vec4 u_Origin;
  uniform mat4 u_ModelMatrix;
  varying vec4 v_Color;
  attribute vec4 a_Color;
  void main() {
    gl_Position = u_ModelMatrix * a_Position + u_Origin;
    v_Color = a_Color;
  }
`;

const FSHADER = `
  // attributes cannot be defined in a fragment shader
  precision mediump float;
  varying vec4 v_Color;
  void main() {
    gl_FragColor = v_Color;
  }
`;

const TEX_VSHADER = `
precision mediump float;
uniform mat4 u_ModelMatrix;
uniform vec4 u_Origin;
attribute vec4 a_Position;
attribute vec2 a_TexCoord;
varying vec2 v_TexCoord;
void main() {
  gl_Position = u_ModelMatrix * a_Position + u_Origin;
  v_TexCoord = a_TexCoord;
}
`

const TEX_FSHADER = `
precision mediump float;
uniform sampler2D u_Sampler;
varying vec2 v_TexCoord;
void main() {
  gl_FragColor = texture2D(u_Sampler, v_TexCoord);
}
`

var gl, shader, scene, renderer, camera, inputHandler
var image_loaded = false;
function main() {
  var canvas = document.getElementById("webgl")
  gl = getWebGLContext(canvas)
  if (!gl) {
    console.log("Failed to get WebGL rendering context")
    return
  }
  shader = new Shader(gl, VSHADER, FSHADER)
  texshader = new Shader(gl, TEX_VSHADER, TEX_FSHADER)

  scene = new Scene()
  inputHandler = new InputHandler(canvas, scene)
  camera = null
  renderer = new Renderer(scene, camera)
  renderer.kickoff()
  renderer.play()

  loadImage('./galaxy.jpg', function(image) {
    scene.addGeometry(new TexCube(0.5, 0.5, 0.2, image))
  })

  loadImage('./one-eye-dog.jpg', function(image) {
    scene.addGeometry(new TexCube(-0.5, 0.5, 0.2, image))
  })
}
