var _inputHandler = null;
var drawing_mode = 'squares'
var color_mode = 'rainbow'
var mouse_down = false

class InputHandler {
  constructor(canvas, scene) {
    this.canvas = canvas;
    this.scene = scene;

    _inputHandler = this;

    // Mouse Events
    this.canvas.onmousedown = function(ev) { mouse_down = true; _inputHandler.click(ev) }
    this.canvas.onmouseup = function() { mouse_down = false }
    this.canvas.onmousemove = function(ev) { if (mouse_down) _inputHandler.click(ev) }

    var color_mode_button = document.getElementById('color_mode')
    color_mode_button.onclick = function() {
      if (color_mode == 'rainbow') {
        color_mode_button.innerHTML = 'Solid Color'
        color_mode = 'solid'
      } else {
        color_mode_button.innerHTML = 'Rainbow'
        color_mode = 'rainbow'
      }
    }

    document.getElementById('add_obj').onclick = function() {
      var filename = document.getElementById('obj_file').value
      console.log(filename)

      if (!filename) {
        document.getElementById('obj_error').innerHTML = "error: please choose a file first"
        return
      }

      document.getElementById('obj_error').innerHTML = ''

      loadFile(filename, function(filestring) {
        console.log('loaded ' + filename)
        if (!filestring) {
          console.error(filename + " is empty or doesn't exist")
          return
        }

        scene.addGeometry(new LoadedOBJ(shader, filestring))
      })
    }

    function handleTextureSelect(evt) {
      var files = evt.target.files
      if (files.length == 0)
        return
      var file = files[0]
      var reader = new FileReader()
      reader.onload = function(e) {
        loadImage(URL.createObjectURL(file), function(image) {
          _inputHandler.image = image
        })
      }
      reader.readAsDataURL(file)
    }

    document.getElementById('tex_file')
      .addEventListener('change', handleTextureSelect, false)

    document.getElementById('clear_tex').onclick = function() {
      document.getElementById('tex_file').value = null
      _inputHandler.image = null
    }
    document.getElementById('clear').onclick = function() { scene.clearGeometry() }
    document.getElementById('play').onclick = function() { renderer.play() }
    document.getElementById('pause').onclick = function() { renderer.pause() }
    document.getElementById('squares').onclick = function() { drawing_mode = 'squares' }
    document.getElementById('triangles').onclick = function() { drawing_mode = 'triangles' }
    document.getElementById('circles').onclick = function() { drawing_mode = 'circles' }
    document.getElementById('cubes').onclick = function() { drawing_mode = 'cubes' }
  }

  getActiveColor() {
    return [
      parseFloat(document.getElementById('red_color').value),
      parseFloat(document.getElementById('green_color').value),
      parseFloat(document.getElementById('blue_color').value),
      1.0
    ]
  }

  getActiveSize(){
    return 1 * document.getElementById('shape_size').value
  }

  getActiveSegments(){
    return 1 * document.getElementById('segments_input').value
  }

  getColors(n) {
    var colors = []
    for (var i=0; i<n; i++) {
      if (color_mode == 'rainbow') {
        for (var j=0; j<3; j++) colors.push(Math.random())
        colors.push(1.0)
      }

      else
        colors = colors.concat(this.getActiveColor())
    }
    return colors
  }

  addShape(x, y) {
    var size = this.getActiveSize()
    var _scene = this.scene;
    switch (drawing_mode) {
      case 'squares':
        _scene.addGeometry(new Square(shader, this.getColors(4), size, x, y))
        break

      case 'triangles':
        _scene.addGeometry(new Triangle(shader, this.getColors(3), size, x, y))
        break

      case 'circles':
        var s = this.getActiveSegments()
        _scene.addGeometry(new Circle(shader, this.getColors(s), size, s, x, y))
        break

      case 'cubes':
        var colors = this.getColors(8)
        if (this.image)
          _scene.addGeometry(new TexCube(x, y, size, this.image))
        else
          _scene.addGeometry(new Cube(shader, colors, size, x, y))

        break
    }
  }

  click(ev) {
    var x = ev.clientX
    var y = ev.clientY
    var rect = ev.target.getBoundingClientRect()
    var x = ((x - rect.x) - (rect.width / 2)) / (rect.width/2)
    var y = ((rect.height/2) - (y - rect.y)) / (rect.height/2)
    console.log("click: ", x, y);
    this.addShape(x, y)
  }
}
