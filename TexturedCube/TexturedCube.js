// TexturedQuad.js (c) 2012 matsuda and kanda
// Vertex shader program
var VSHADER_SOURCE = `
uniform mat4 u_Matrix;
attribute vec4 a_Position;
attribute vec2 a_TexCoord;
varying vec2 v_TexCoord;
void main() {
  gl_Position = u_Matrix * a_Position;
  v_TexCoord = a_TexCoord;
}
`

// Fragment shader program
var FSHADER_SOURCE = `
#ifdef GL_ES
precision mediump float;
#endif
uniform sampler2D u_Sampler;
varying vec2 v_TexCoord;
void main() {
  gl_FragColor = texture2D(u_Sampler, v_TexCoord);
}
`

function main() {
  var canvas = document.getElementById('webgl');
  var gl = getWebGLContext(canvas);
  initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)
  var n = initVertexBuffers(gl);
  gl.clearColor(0.0, 0.0, 0.0, 1.0);
  initTextures(gl, n)
  gl.enable(gl.DEPTH_TEST)
  gl.enable(gl.POLYGON_OFFSET_FILL)
}

function initVertexBuffers(gl) {
  var verticesTexCoords = new Float32Array([
    // back
    1,1,-1,0,3,    1,-1,-1,0,0,   -1,1,-1,3,3,
    -1,1,-1,3,3,   1,-1,-1,0,0,   -1,-1,-1,3,0,

    // front
    -1,1,1, 0,1,   -1,-1,1,0,0,   1,-1,1,2,0,
    -1,1,1, 0,1,    1,-1,1,2,0,   1,1,1,2,1,

    // top
    -1,1,-1, 0,1,   1,1,1, 1,0,   1,1,-1, 1,1,
    -1,1,-1, 0,1,   -1,1,1,0,0,   1,1,1,1,0,

    // left
    -1,1,-1,0,1,    -1,-1,-1,0,0.5,   -1,-1,1,1,0.5,
    -1,1,-1,0,1,    -1,1,1,1,1,       -1,-1,1,1,0.5,

    // bottom
    -1,-1,1,0,0,    -1,-1,-1,0,1,     1,-1,-1,1,1,
    -1,-1,1,0,0,    1,-1,1,1,0,       1,-1,-1,1,1,

    // right
    1,1,1,0,0.5,    1,-1,1,0,0,       1,-1,-1,1,0,
    1,1,1,0,0.5,    1,1,-1,1,0.5,     1,-1,-1,1,0,

  ]);
  var n = 36;


  var mvpMatrix = new Matrix4()
  mvpMatrix.setPerspective(30,1,1,100)
  mvpMatrix.lookAt(7,3,5, 0,0,0,  0,1,0)
  mvpMatrix.rotate(0, 0, 1, 0)
  gl.uniformMatrix4fv(gl.getUniformLocation(gl.program, 'u_Matrix'), false, mvpMatrix.elements)

  // Create the buffer object
  var vertexTexCoordBuffer = gl.createBuffer();

  // Bind the buffer object to target
  gl.bindBuffer(gl.ARRAY_BUFFER, vertexTexCoordBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, verticesTexCoords, gl.STATIC_DRAW);

  var FSIZE = verticesTexCoords.BYTES_PER_ELEMENT;
  //Get the storage location of a_Position, assign and enable buffer
  var a_Position = gl.getAttribLocation(gl.program, 'a_Position');
  gl.vertexAttribPointer(a_Position, 3, gl.FLOAT, false, FSIZE * 5, 0);
  gl.enableVertexAttribArray(a_Position);  // Enable the assignment of the buffer object

  // Get the storage location of a_TexCoord
  var a_TexCoord = gl.getAttribLocation(gl.program, 'a_TexCoord');
  // Assign the buffer object to a_TexCoord variable
  gl.vertexAttribPointer(a_TexCoord, 2, gl.FLOAT, false, FSIZE * 5, FSIZE * 3);
  gl.enableVertexAttribArray(a_TexCoord);  // Enable the assignment of the buffer object

  return n;
}

function initTextures(gl, n) {
  var texture = gl.createTexture();   // Create a texture object

  // Get the storage location of u_Sampler
  var u_Sampler = gl.getUniformLocation(gl.program, 'u_Sampler');
  console.log(u_Sampler)
  var image = new Image();  // Create the image object
  // Register the event handler to be called on loading an image
  image.onload = function(){ loadTexture(gl, n, texture, u_Sampler, image); };
  // Tell the browser to load an image
  image.crossOrigin = 'anonymous'
  image.src = './galaxy.jpg'

  return true;
}

function loadTexture(gl, n, texture, u_Sampler, image) {
  gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1); // Flip the image's y axis
  // Enable texture unit0
  gl.activeTexture(gl.TEXTURE0);
  // Bind the texture object to the target
  gl.bindTexture(gl.TEXTURE_2D, texture);

  // Set the texture parameters
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);

  // Set the texture image
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, image);

  // Set the texture unit 0 to the sampler
  gl.uniform1i(u_Sampler, 0);

  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);   // Clear <canvas>

  gl.drawArrays(gl.TRIANGLES, 0, n);

}
