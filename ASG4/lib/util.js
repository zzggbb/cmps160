function mergeVertsColors(v, c) {
  var vc = []
  for (var i=0; i<v.length/3; i++) {
    for (var j=0; j<3; j++) vc.push(v[i*3 + j])
    for (var j=0; j<4; j++) vc.push(c[i*4 + j])
  }
  return new Float32Array(vc)
}

function loadImage(source, success, fail) {
  var image = new Image()
  image.crossOrigin = 'anonymous'
  image.src = source
  image.onload = function() {
    success(image)
  }
  image.onerror = function() {
    fail()
  }
}
