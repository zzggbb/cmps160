var _renderer = null;
class Renderer {
  constructor(scene, program) {
    this.scene = scene;
    gl.clearColor(0.0, 0.0, 0.0, 1.0); // Setting canvas' clear color
    _renderer = this;
    this.paused = true
    this.eye = {x: 0, y: -13, z: 0}
    this.program = program
    this.frame_last = Date.now()
    this.frame_rate = null

    this.tilt = new Matrix4()
    this.view = new Matrix4()
    this.proj = new Matrix4()
    this.view_loc = gl.getUniformLocation(this.program, 'u_ViewMatrix')
    this.proj_loc = gl.getUniformLocation(this.program, 'u_ProjMatrix')
  }
  kickoff() { _renderer.render(); requestAnimationFrame(_renderer.kickoff); }
  play() { this.paused = false }
  pause() { this.paused = true }
  render() {
    if (this.paused) return

    var now = Date.now()
    this.frame_rate = 1000/(now - this.frame_last)
    this.frame_last = now

    this.tilt.setIdentity()
    this.tilt.rotate(yaw, 0, 1, 0)
    this.tilt.rotate(-pitch, 1, 0, 0)

    var vel_relative = this.tilt.multiplyVector3(new Vector3([velocity.x, velocity.y, velocity.z]))
    this.eye.x += vel_relative.elements[0]
    this.eye.y += vel_relative.elements[1]
    this.eye.z += vel_relative.elements[2]

    var at_relative = this.tilt.multiplyVector3(new Vector3([0, 0, -1]))
    var at = new Vector3([
      this.eye.x + at_relative.elements[0],
      this.eye.y + at_relative.elements[1],
      this.eye.z + at_relative.elements[2],
    ])

    var up = this.tilt.multiplyVector3(new Vector3([0, 1, 0]))

    this.view.setLookAt(
      this.eye.x, this.eye.y, this.eye.z,
      at.elements[0], at.elements[1], at.elements[2],
      up.elements[0], up.elements[1], up.elements[2]
    )

    gl.uniformMatrix4fv(this.view_loc, false, this.view.elements)

    if (projection_mode == 'perspective')
      this.proj.setPerspective(fov, 1, 1, 200)
    else
      this.proj.setOrtho(-100, 100, -100, 100, -100, 100)

    gl.uniformMatrix4fv(this.proj_loc, false, this.proj.elements)

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    for (var i = 0; i < this.scene.geometries.length; i++) {
      this.scene.geometries[i].render()
    }
  }
}
