class TexCube {
  constructor(x, y, z, size, image) {
    this.x = x
    this.y = y
    this.z = z
    this.size = size
    this.image = image
    this.vdata = new Float32Array([
      // back
      1,1,-1,  .75,.5,    1,-1,-1, .75,.25,   -1,1,-1,   1,.5,
      -1,1,-1, 1,.5,    1,-1,-1,   .75,.25,   -1,-1,-1,  1,.25,
      // front
      -1,1,1, .25,.5,   -1,-1,1, .25,.25,   1,-1,1, .5,.25,
      -1,1,1, .25,.5,    1,-1,1, .5,.25,   1,1,1, .5,.5,
      // top
      -1,1,-1, .25, .75,   1,1,1, .5,.5,   1,1,-1, .5, .75,
      -1,1,-1, .25, .75,  -1,1,1, .25, .5,   1,1,1,  .5,.5,
      // left
      -1,1,-1, 0,.5,    -1,-1,-1, 0,.25,    -1,-1,1, .25,.25,
      -1,1,-1, 0,.5,    -1,1,1,   .25,.5, -1,-1,1, .25,.25,
      // bottom
      -1,-1,1,.25,.25,    -1,-1,-1, .25,0,     1,-1,-1, .5,0,
      -1,-1,1,.25,.25,    1,-1,1,   .5,.25,    1,-1,-1, .5,0,
      // right
      1,1,1, .5,.5,    1,-1,1,  .5,.25,       1,-1,-1,  .75,.25,
      1,1,1, .5,.5,    1,1,-1,  .75,0.5,     1,-1,-1,   .75,.25,
    ]);
    this.FSIZE = this.vdata.BYTES_PER_ELEMENT;
    this.n = this.vdata.length/5;
    this.vbuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.vbuffer);
    gl.bufferData(gl.ARRAY_BUFFER, this.vdata, gl.STATIC_DRAW);

    // setup texture
    this.texture = gl.createTexture()
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1)
    gl.activeTexture(gl.TEXTURE0)
    gl.bindTexture(gl.TEXTURE_2D, this.texture)
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
    gl.uniform1i(gl.getUniformLocation(program, 'u_Sampler'), 0)
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, this.image)

    // source the 'a_Position' attribute from the vdata
    var a_Position = gl.getAttribLocation(program, 'a_Position');
    gl.vertexAttribPointer(a_Position, 3, gl.FLOAT, false, this.FSIZE * 5, 0);
    gl.enableVertexAttribArray(a_Position);

    // source the 'a_TexCoord' attribute from the vdata
    var a_TexCoord = gl.getAttribLocation(program, 'a_TexCoord');
    gl.vertexAttribPointer(a_TexCoord, 2, gl.FLOAT, false, this.FSIZE * 5, this.FSIZE * 3);
    gl.enableVertexAttribArray(a_TexCoord);

    this.m = new Matrix4()
    this.m.setIdentity()
    this.m.translate(this.x, this.y, this.z)
    this.m.scale(this.size, this.size, this.size)
    this.m_loc = gl.getUniformLocation(program, 'u_ModelMatrix')
  }

  render() {
    gl.bindTexture(gl.TEXTURE_2D, this.texture)
    gl.bindBuffer(gl.ARRAY_BUFFER, this.vbuffer);
    gl.uniformMatrix4fv(this.m_loc, false, this.m.elements)
    gl.drawArrays(gl.TRIANGLES, 0, this.n)
  }
}
